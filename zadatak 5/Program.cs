﻿using System;

namespace zadatak5
{
	class Program
	{
		static void Main(string[] args)
		{
		
			AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
			FileLogger fileLogger =
			new FileLogger(MessageType.ERROR | MessageType.WARNING, @"C:\Users\perok\Desktop\logFile.txt");
			logger.SetNextLogger(fileLogger);

			logger.Log("Error", MessageType.ERROR);
			logger.Log("Information", MessageType.INFO);
			logger.Log("Warning", MessageType.WARNING);
			logger.Log("All", MessageType.ALL);
		}
	}
}
