﻿using System;

namespace zadatak3
{
	class Program
	{
		static void Main(string[] args)
		{
			CareTaker caretaker = new CareTaker();
			DateTime time = new DateTime(2020, 5, 14);
			ToDoItem toDoItem = new ToDoItem("title","text",time);
			caretaker.AddMemento(toDoItem.StoreState());

			toDoItem.ChangeTask("title 2");
			Console.WriteLine(toDoItem.ToString());

			caretaker.AddMemento(toDoItem.StoreState());
			toDoItem.ChangeTask("title 3");
			Console.WriteLine(toDoItem.ToString());

			toDoItem.RestoreState(caretaker.GetMemento(1));
			Console.WriteLine(toDoItem.ToString());

			caretaker.RemoveMementoAtIndex(1);


		}
	}
}
