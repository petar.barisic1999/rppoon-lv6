﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace zadatak3
{
	class CareTaker
	{
		private List<Memento> mementos;
		public CareTaker()
		{
			this.mementos = new List<Memento>();
		}

		public void AddMemento(Memento memento )
		{
			mementos.Add(memento);

		}
		public void RemoveMementoAtIndex(int index)
		{
			mementos.RemoveAt(index);
		}
		public void CountMementos()
		{
			mementos.Count();
		}
		public Memento GetMemento(int index)
		{
			return mementos[index];
		}
		
	}
}
