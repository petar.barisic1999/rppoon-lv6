﻿using System;
using System.Collections.Generic;

namespace zadatak4
{
	class Program
	{
		static void Main(string[] args)
		{
			List<Memento> careTaker = new List<Memento>();
			BankAccount newAccount = new BankAccount("James Bond", "Chelsea", 100000);
			careTaker.Add(newAccount.StoreState());
			Console.WriteLine(newAccount.ToString());

			newAccount.ChangeOwnerAddress("Manchester");
			newAccount.UpdateBalance(100000);
			careTaker.Add(newAccount.StoreState());
			Console.WriteLine(newAccount.ToString());

			
			newAccount.RestoreState(careTaker[0]);
			Console.WriteLine(newAccount.ToString());
		}
	}
}
