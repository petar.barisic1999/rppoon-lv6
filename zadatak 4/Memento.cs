﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak4
{
	class Memento
	{
        public string OwnerName { get; private set; }
        public string OwnerAddress { get; private set; }
        public decimal Balance { get; private set; }
        public Memento(string ownername, string owneraddress, decimal balance)
        {
            this.OwnerName = ownername;
            this.OwnerAddress = owneraddress;
            this.Balance = balance;
            
        }
    }
}

