﻿using System;

namespace zadatak1
{
	class Program
	{
		static void Main(string[] args)
		{
            Notebook myNotebook = new Notebook();
            myNotebook.AddNote(new Note("Title 1", "text 1"));
            myNotebook.AddNote(new Note("Title 2", "text 2"));
            myNotebook.AddNote(new Note("Title 3", "text 3"));

            IAbstractIterator iterator = myNotebook.GetIterator();
            while (iterator.IsDone != true)
            {
              iterator.Current.Show();
              iterator.Next();
            }
        }
    }
}
