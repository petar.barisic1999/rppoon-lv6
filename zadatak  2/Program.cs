﻿using System;

namespace zadatak2
{
	class Program
	{
		static void Main(string[] args)
		{
            Box box = new Box();
            box.AddProduct(new Product("Ball",1.5));
            box.AddProduct(new Product("Shirt",2.7));
            box.AddProduct(new Product("Shoes",3.5));

            IAbstractIterator iterator = box.GetIterator();
            while (iterator.IsDone != true)
            {
                Console.WriteLine(iterator.Current.ToString());
                iterator.Next();
            }
        }
	}
}
