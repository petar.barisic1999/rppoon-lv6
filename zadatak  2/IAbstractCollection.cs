﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak2
{
	interface IAbstractCollection
	{
		IAbstractIterator GetIterator();
	}
}
